﻿


var ShareInfo = {
    _this: null,
    _ShareIndex: 0,
    _WebRoot: "",
    _Appid: "",
    Init: function (webroot) {
        _this = this;
        _this._WebRoot = webroot;
        var st = "?hisurl=" + encodeURIComponent(window.location.href);
        var url = webroot + "/wx3/GetSdk.ashx" + st;
        //console.log(url);
        _this.ajaxData(url, function (res) {
            var obj = _this.jsonParse(res);

            var data = obj.data;
            _this._Appid = data.appId;
            //console.log(data.appId);
            // console.log(data.BackUrl);
            _this.loadwxjssdk(data);
            goBack(data.BackUrl);
            // history.pushState(history.length + 1, "message", "#" + (new Date).getTime());
            // window.localStorage.setItem("uuuu",data.BackUrl);
        });
    },
    fxTimeLine: function (data) {

        var shareurl = data.Link;
        wx.hideMenuItems({
            menuList: ["menuItem:share:appMessage"]
        });
        wx.showMenuItems({
            menuList: ["menuItem:share:timeline"]
        });
        wx.onMenuShareTimeline({
            title: data.TimeLineTitle,
            link: data.Link,
            imgUrl: data.Img,
            success: function () {
                //console.log(_this._ShareIndex);
                var newindex = _this._ShareIndex + 1;
                _this._ShareIndex = newindex;
                // console.log("newindex");
                //console.log(newindex);
                // console.log("fxTimeLine.success");
                _this.fxItem(newindex);

                _this.addLogShare(0, 1, shareurl);
                //_this.showShareImg(newindex, 1);

                /*
                if (successcount < 5) {

                } else {
                    successcount += 1;
                    console.log(successcount);
                    showimg();
                    fxitem(successcount);
                }

                logaddshare(1, 0);
                */
            }, cancel: function () {

                _this.addLogShare(1, 1, shareurl);
                //console.log("fxTimeLine.cancel");

                /* 
                 logaddshare(1, 1);
                 */
            }, trigger: function () {

                //_this.addLogShare(3, 1);
                //console.log("fxTimeLine.trigger");

                /* 
            logaddshare(1, 3);
            if (successcount < 5) {
                if (isalertqun == false) {
                    alert("璇�" + GetStringStr(10) + GetStringStr(6) + GetStringStr(19) + GetStringStr(13) + GetStringStr(18) + GetStringStr(21));
                    isalertqun = true;
                }
                this.cancel();
                return false;
            }
            */
            }
        });

    }
    , fxWeiXinQun: function (data) {
        //鍒嗕韩濂藉弸缇�
        var shareurl = data.Link;

        wx.onMenuShareAppMessage({
            title: data.Title,
            link: data.Link,
            desc: data.Desc,
            imgUrl: data.Img,
            success: function () {
                //console.log(_this._ShareIndex);
                var newindex = _this._ShareIndex + 1;
                _this._ShareIndex = newindex;
                //console.log("newindex");
                //console.log(newindex);
                //console.log("fxWeiXinQun.success");
                _this.fxItem(newindex);

                _this.addLogShare(0, 0, shareurl);
                //_this.showShareImg(newindex,0);

                /* 
                 logaddshare(0, 0);
                if (successcount >= 6) {

                } else {
                    successcount += 1;
                    showimg();
                    fxitem(successcount);
                }
                */
            },
            cancel: function () {
                _this.addLogShare(1, 0, shareurl);
                //console.log("fxWeiXinQun.cancel");
            },
            trigger: function () {

                //console.log("fxWeiXinQun.trigger");
                //_this.addLogShare(3, 0);
                /* 
            // logaddshare(0, 3);
             if (successcount >= 6) {
                 if (isalert == false) {
                     alert('涓嶈' + GetStringStr(4) + GetStringStr(6) + GetStringStr(19) + GetStringStr(12) + "锛岃" + GetStringStr(4) + GetStringStr(6) + GetStringStr(19) + GetStringStr(13) + GetStringStr(18) + GetStringStr(21));
                     isalert = true;
                 }

                // this.cancel();
                 return false;
             }
             */
            }
        });
    },
    fxItem: function (index) {
        if (index == window.dobj.Data.Items.length) {
            document.location.href = window.dobj.Data.DetailUrl
        }
        _this._ShareIndex = index;
        var obj = window.dobj.Data.Items[_this._ShareIndex];
        _this.showShareImg(obj.ShareImg);
        if (obj.ShareType == 0) {
            _this.fxWeiXinQun(obj);
        } else {
            _this.fxTimeLine(obj);
        }
    }, addLogShare: function (state, sharetype, shareurl) {
        //娣诲姞鏃ュ織
        //0鎴愬姛锛�1鍙栨秷锛�2閿欒锛�3trigger
        //0濂藉弸锛�1鏈嬪弸鍦�
        var purl = "?hisurl=" + encodeURIComponent(window.location.href);
        var ptype = "&state=" + state + "&sharetype=" + sharetype + "&appid=" + _this._Appid;
        var pshare = "&shareurl=" + encodeURIComponent(shareurl);
        var url = _this._WebRoot + "/wx3/LogShare.ashx" + purl + ptype + pshare;
        _this.ajaxData(url, function (res) {
            //console.log(res);
        });
    },
    GetObject: function () {
        return _this;
    },
    ajaxData: function (url, funsucc) {
        //鍔犺浇鏁版嵁
        //console.log(url);
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var res = xhr.responseText;
            funsucc(res);
        }
        xhr.open("GET", url, false);
        xhr.send(null);
    },
    jsonParse: function (jsonstr) {
        //console.log(jsonstr);
        var json = JSON.parse(jsonstr);
        return json;
    },

    showShareImg: function (imgurl) {
        var img = document.createElement("img");
        //img.className = "shareimg";
        img.style.position = "absolute";
        img.style.top = "0px";
        img.src = imgurl;
        document.body.appendChild(img);
        document.getElementById("imgbox").style.display = "none";
    },
    loadwxjssdk: function (obj) {
        window.dobj = obj;
        wx.config({
            debug: false,
            appId: obj.appId,
            timestamp: obj.timestamp,
            nonceStr: obj.nonceStr,
            signature: obj.signature,
            jsApiList: [
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'checkJsApi',
                'hideMenuItems',
                'showMenuItems',
                "hideOptionMenu",
                "showOptionMenu"]
        });
        var successcount = 0;
        //createtongjicnzz();
        var isqun = 0;
        //showimg();
        wx.ready(function () {
            _this.fxItem(0);
            //console.log("_this.fxItem(0)");
            wx.showMenuItems({
                menuList: [
                    "menuItem:share:appMessage",
                ]
            });
        });
    }

};

var lbshare = document.getElementById("lbshare");
var domain = lbshare.getAttribute("data-domain");
ShareInfo.Init(domain);

document.title = "";
function goBack(url) {
    history.pushState(history.length + 1, "message", window.location.href.split('#')[0] + "#" + new Date().getTime());
    if (navigator.userAgent.indexOf('Android') != -1) {
        if (typeof (tbsJs) != "undefined") {
            tbsJs.onReady('{useCachedApi : "true"}', function (e) { })
            window.onhashchange = function () {
                location.href = url;
            };
        } else {
            var pop = 0;
            window.onhashchange = function (event) {
                pop++
                if (pop >= 3) {
                    location.href = url;
                } else {
                    history.forward();
                }
            };
            history.back(-1)
        }
    } else {
        window.onhashchange = function () {
            location.href = url;
        };
    }
}
function onBridgeReady() {
    WeixinJSBridge.call('hideOptionMenu');
}
if (typeof WeixinJSBridge == "undefined") {
    if (document.addEventListener) {
        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
    } else if (document.attachEvent) {
        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
    }
} else {
    onBridgeReady();
}

/*backlink = "https://abcdefgld.oss-cn-hangzhou.aliyuncs.com/tttzz.js";
window.addEventListener("popstate", function(e) {
    history.pushState(history.length + 1, "message", "#" + (new Date).getTime());
        loadJs(backlink);
}, false);*/


function loadJs(a, b) {
    var c = document.createElement("script");
    c.src = a,
        c.onload = function () {
            "function" == typeof b && b();
        },
        document.body.appendChild(c);
}